import Vue from 'vue'
import axios from 'axios'
import config from './config'

axios.defaults.baseURL = config.api; // 设置axios的基础请求路径
axios.defaults.timeout = 5000; // 设置axios的请求时间
axios.defaults.withCredentials = true;
axios.interceptors.request.use(config => {
        let token = sessionStorage.getItem("Admin_Token");
        if (token) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
            config.headers.token = `${token}`;
        }
        // if (config.url.indexOf(url) === -1) {
        //     config.url = url + config.url;/*拼接完整请求路径*/
        // }
        return config;
    },
    err => {
        return Promise.reject(err);
    });

axios.loadData = async function (url) {
  const resp = await axios.get(url);
  return resp.data;
}

Vue.prototype.$http = axios;// 将axios添加到 Vue的原型，这样一切vue实例都可以使用该对象

