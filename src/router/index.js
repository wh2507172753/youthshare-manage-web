import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

function route(path, file, name, children) {
  return {
    exact: true,
    path,
    name,
    children,
    component: () => import('../pages' + file)
  }
}

export default new Router({
  routes: [
    route("/login", '/Login', "Login"),// /login路径，路由到登录组件
    {
      path: "/", // 根路径，路由到 Layout组件
      component: () => import('../pages/Layout'),
      redirect: "/login",
      // redirect:"/index/dashboard",
      children: [ // 其它所有组件都是 Layout的子组件
        route("/index/dashboard", "/Dashboard", "Dashboard"),
        route("/shop/category", '/shop/Category', "Category"),
        route("analysis/shop/analysis", '/shop/analysis', "Analysis"),
        route("/shop/goods", '/shop/Goods', "Goods"),
        route("/share/TopicClassify", '/share/TopicClassify', "TopicClassify"),
        route("/share/Topic", '/share/Topic', "Topic"),
        route("/share/Message", '/share/Messages', "Message"),
        route("/share/Host", '/share/Host', "Host"),
        route("/share/HotSearch", '/share/HotSearch', "HotSearch"),
        route("analysis/share/analysis", '/share/analysis', "analysis"),
        route("/user/Users", '/user/Users', "User"),
        route("/user/Identity", '/user/Identity', "Identity"),
        route("/user/Famous", '/user/Famous', "Famous"),
        route("/user/Star", '/user/Star', "Star"),
        route("/user/Complain", "/user/Complain", "Complain"),
        route("/user/Feedback", "/user/Feedback", "Feedback"),
        route("/analysis/user/Statistics", "/user/Statistics", "Statistics"),
        route("/trade/promotion", '/trade/Promotion', "Promotion"),
        route("/trade/order", '/trade/Order', "Order"),
      ]
    }
  ]
})
