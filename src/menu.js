var menus = [
  // {
  //   action: "home",
  //   title: "首页",
  //   path: "/index",
  //   items: [{title: "统计", path: "/dashboard"}]
  // },
  {
    action: "apps",
    title: "换购好物模块",
    path: "/shop",
    items: [
      {title: "分类管理", path: "/category"},
      {title: "商品列表", path: "/goods"},
    ]
  },

  {
    action: "attach_money",
    title: "分享模块",
    path: "/share",
    items: [
      {title: "话题分类管理", path: "/TopicClassify"},
      {title: "话题管理", path: "/Topic"},
      {title: "微博管理", path: "/Message"},
      // {title: "统计分析", path: "/analysis"},
      {title: "主持人申请", path: "/Host"},
      {title: "热搜管理", path: "/HotSearch"},
    ]
  },
  {
    action: "people",
    title: "用户管理",
    path: "/user",
    items: [
      {title: "用户管理", path: "/Users"},
      {title: "身份认证", path: "/Identity"},
      {title: "名人认证", path: "/Famous"},
      {title: "明星认证", path: "/Star"},
      {title: "用户投诉", path: "/Complain"},
      {title: "用户反馈", path: "/Feedback"},

    ]
  },
  {
    action: "people",
    title: "统计分析",
    path: "/analysis",
    items: [
      {title: "换购好物统计", path: "/shop/analysis"},
      {title: "分享社区统计", path: "/share/analysis"},
      {title: "用户统计", path: "/user/Statistics"}
    ]
  }
]

export default menus;
